# 这里是干嘛用的，是连接mysql用的配置文件，执行数据库操作，
# 记得要去setting文件配置django连接这里的操作，还要在__init__.py文件改掉数据库的默认驱动
from unittest import result
from pymysql import cursors,connect
# 连接数据库cursors是游标，connect是建立连接
conn = connect(host='127.0.0.1',
user='root',
password='123456',
db='guest',
charset='utf-8',
cursorclass=cursors.DictCursor)
# 
try:
    #创建conn的浮标？
    with conn.cursor() as cursor:
        # 创建嘉宾数据
        sql = "insert into sign_guest (realname,phone,email,sign,event_id,create_time) values \
        ('tom','3216456','111@qq.com',0,1,NOW())"
        # 执行数据库命令
        cursor.execute(sql)
        # 再提交给数据库执行
        conn.commit()
    with conn.cursor() as cursor:
        # 查询嘉宾
        sql = "select * from sign_guest where phone=%d)"
        # 执行数据库命令
        result=cursor.execute(sql,('3216456'))
        # 查询不需要提交到数据库执行
        print(result)
finally:
    conn.close()
