from django.db import models

# Create your models here.
# User表
class User(models.Model):
    user_id = models.IntegerField(unique=True)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length = 100)
    create_time = models.DateTimeField(auto_now_add=True)
    def __str__(self) -> str:
        return super().__str__()
    # 按创建时间排序
    class Meta:
        ordering = ['create_time']
# Role表
class Role(models.Model):
    role_id = models.IntegerField(unique=True)
    role = models.CharField(max_length=100)
    create_time = models.DateTimeField(auto_now_add=True)
    def __str__(self) -> str:
        return super().__str__()
    # 按创建时间排序
    class Meta:
        ordering = ['create_time']
# 权限表
class Authority(models.Model):
    authority_id = models.IntegerField(unique=True)
    authority = models.CharField(max_length=100)
    create_time = models.DateTimeField(auto_now_add=True)
    def __str__(self) -> str:
        return super().__str__()
    # 按创建时间排序
    class Meta:
        ordering = ['create_time']
#用户角色中间表
class UserRole(models.Model):
    user_id = models.IntegerField()
    role_id = models.IntegerField()
    def __str__(self) -> str:
        return super().__str__()
#角色权限中间表
class RoleAuthority(models.Model):
    role_id = models.IntegerField()
    authority_id = models.IntegerField()
    def __str__(self) -> str:
        return super().__str__()