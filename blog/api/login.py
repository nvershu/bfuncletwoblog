from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from django.db import connection
# Create your views here.
# 登录
# 新建一个login方法，在这里做登录操作,登录成功后要跳转到manage页面
def login(request):
    print(request.GET.get('name',''))
    # 如果是post那就是注册
    if request.method == 'POST':
        username = request.POST.get('username',' ')
        password = request.POST.get('password',' ')
        # 可以用django自带的权限认证去做登录认证
        # user = auth.authenticate(username=username, password=password)
        if username == 'admin' and password == 'admin123':
            # 登录成功后直接重定向manage方法在渲染一个页面
            response = HttpResponseRedirect('/event_manage/')
            # 这里可以保存到cookis或者session
            response.set_cookie('user',username)
            # 保存session，在django里面保存session一定要创建一个表去保存session
            request.session['user'] = username
            return response
        else:
            return render(request,'index.html',{'error':'username or password error'})
    # 不然就是登录
    elif request.method == 'GET':
        username = request.GET.get('username',' ')
        password = request.GET.get('password',' ')
        cursor = connection.cursor()
        cursor.execute('select * from user where 1=1')
        rows = cursor.fetchall()
        print(rows)
        print(request.GET)
    data={
    'username':username,
    'password':password
    }
    return JsonResponse({'code': 400,'data':data, 'message': '返回信息'})
    # return render(request,'index.html',{'error':'username or password error'})

