from email import message
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from django.db import connection
import jwt
import time

 # 可以用django自带的权限认证去做登录认证
cursor = connection.cursor()
# Create your views here.
# 登录
# 新建一个login方法，在这里做登录操作,登录成功后要跳转到manage页面
def login(request):
    message = '注册成功!即将跳转到主页'
    username = ''
    password = ''
    email = ''
    # 如果是post那就是注册
    if request.method == 'POST':
        # 如果是json传数据要
        username = request.POST.get('username','')
        password = request.POST.get('password','')
        email = request.POST.get('email','')
        print('aaa',username,email)
        if(email == '' or username == '' or password == ''):
            return JsonResponse({'code': 400,'data':[], 'message': '参数有误'})
        else:
            # 获取数据库的用户名和密码
            cursor.execute('select * from user where username=%s',username)
            rows = cursor.fetchall()
            print(rows)
        # 只有数据库没有这个用户名，并且前端的用户名密码和email不为空才能插入数据库
        if len(rows) == 0:
            sql = "insert into user (username,password,email) values ('%s','%s','%s')"%(username,password,str(email))
            print('----------',sql)
            cursor.execute(sql)
        else:
            return JsonResponse({'code': 400,'data':[], 'message': '用户已存在'})
        #     # 这里可以保存到cookis或者session
            # response.set_cookie('user',username) #这个操作留给前端去做
    # 不然就是登录
    elif request.method == 'GET':
        username = request.GET.get('username','')
        password = request.GET.get('password','')
        if username == '' or password == '':
            message = '参数有误'
            return JsonResponse({'code': 400,'data':[], 'message': message})
        else:
            print('uername',username)
            # 获取数据库的用户名和密码
            cursor.execute('select * from user where username=%s',username)
            rows = cursor.fetchall()
            print(rows)
        if len(rows) == 0:
            return JsonResponse({'code': 200,'data':[], 'message': '没有该用户'})
    # 生成一个字典，包含我们的具体信息
    d = {
        # 公共声明
        'exp':time.time()+3000, # (Expiration Time) 此token的过期时间的时间戳
        'iat':time.time(), # (Issued At) 指明此创建时间的时间戳
        'iss':'Issuer', # (Issuer) 指明此token的签发者
        # 私有声明
        'data':{
            'username':username,
            'timestamp':time.time()
        }
    }
    jwt_encode = jwt.encode(d,'123456',algorithm='HS256')
    print(jwt_encode)
    # 打印token串
    #eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MjI2MDQ3MTguNjczNjgzNiwiaWF0IjoxNjIyNjAxNzE4LjY3MzY4MzYsImlzcyI6Iklzc3VlciIsImRhdGEiOnsidXNlcm5hbWUiOiJ4amoiLCJ0aW1lc3RhbXAiOjE2MjI2MDE3MTguNjczNjgzNn19.ASgB9-1U9ADhv6AmBH7p8leEtWMTMhaDQJSaZ9z9kZg
    connection.close()
    return JsonResponse({'code': 200,'data':{'token':d,username:username}, 'message': message})
    # return render(request,'index.html',{'error':'username or password error'})

