from django.contrib import admin

from .models import Role, User,Authority

# Register your models here.
admin.site.register(User)
admin.site.register(Role)
admin.site.register(Authority)
